-- MySQL dump 10.13  Distrib 5.5.52, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: testFB2
-- ------------------------------------------------------
-- Server version	5.5.52-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Account`
--

DROP TABLE IF EXISTS `Account`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Account` (
  `AccountNumber` int(11) NOT NULL DEFAULT '0',
  `AccountCreationDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `UserID` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`AccountNumber`),
  KEY `UserID` (`UserID`),
  CONSTRAINT `Account_ibfk_1` FOREIGN KEY (`UserID`) REFERENCES `User` (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Account`
--

LOCK TABLES `Account` WRITE;
/*!40000 ALTER TABLE `Account` DISABLE KEYS */;
INSERT INTO `Account` VALUES (1,'2016-11-07','1'),(2,'2016-11-07','2'),(3,'2016-11-07','2'),(4,'2016-11-07','3'),(5,'2016-11-07','4'),(6,'2016-11-07','3'),(7,'2016-11-07','4'),(8,'2016-11-17','9'),(9,'2016-11-17','8'),(10,'2016-11-17','7');
/*!40000 ALTER TABLE `Account` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Advertisement`
--

DROP TABLE IF EXISTS `Advertisement`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Advertisement` (
  `AdvertisementID` int(11) NOT NULL auto_increment,
  `Company` char(20) DEFAULT NULL,
  `Type` char(20) DEFAULT NULL,
  `ItemName` char(20) DEFAULT NULL,
  `Content` char(140) DEFAULT NULL,
  `EmployeeID` varchar(20) DEFAULT NULL,
  `DateMade` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `UnitPrice` double DEFAULT NULL,
  `Qty` int(11) DEFAULT NULL,
  PRIMARY KEY (`AdvertisementID`),
  KEY `EmployeeID` (`EmployeeID`),
  CONSTRAINT `Advertisement_ibfk_1` FOREIGN KEY (`EmployeeID`) REFERENCES `Employee` (`UserID`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Advertisement`
--

LOCK TABLES `Advertisement` WRITE;
/*!40000 ALTER TABLE `Advertisement` DISABLE KEYS */;
INSERT INTO `Advertisement` VALUES (1,'Sony','Video Gaming','PlayStation 4','Box conains console, Controller, 1x HDMI, 1x Power Adapter','5','2014-09-22',300,100),(2,'Apple','Electronics','Apple MacBook Pro','Box contains laptop and charger','5','2015-11-07',900,50),(3,'kfc','food','family meal','chiken wings','6','2015-11-07',900,50),(4,'staple','office','discount','discount during thanksgiving','7','2015-11-07',900,50),(5,'mc','Electronics','surface','better than mac','8','2015-11-07',900,50),(6,'blizzard','game','diablo','biablo 3 coming out','9','2015-11-07',900,50),(7,'facebook','Electronics','fb phone','better than iphone','10','2015-11-07',900,50),(8,'amazon','shopping','gift card','give your friend a gift card','11','2015-11-07',900,50),(9,'sony','Electronics','xperia','use it under water','12','2015-11-07',900,50),(10,'google','Electronics','pixel','god','13','2015-11-07',900,50);
/*!40000 ALTER TABLE `Advertisement` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Comment`
--

DROP TABLE IF EXISTS `Comment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Comment` (
  `CommentID` int(11) NOT NULL AUTO_INCREMENT,
  `PostID` int(11) DEFAULT NULL,
  `Content` char(140) DEFAULT NULL,
  `CDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `AuthorID` varchar(20) DEFAULT NULL,
  `LikeCounter` int(11) DEFAULT NULL,
  PRIMARY KEY (`CommentID`),
  KEY `AuthorID` (`AuthorID`),
  KEY `PostID` (`PostID`),
  CONSTRAINT `Comment_ibfk_1` FOREIGN KEY (`AuthorID`) REFERENCES `User` (`UserID`),
  CONSTRAINT `Comment_ibfk_2` FOREIGN KEY (`PostID`) REFERENCES `Post` (`PostID`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Comment`
--

LOCK TABLES `Comment` WRITE;
/*!40000 ALTER TABLE `Comment` DISABLE KEYS */;
INSERT INTO `Comment` VALUES (14,18,'chicken','2016-12-07 06:46:54','superman',0),(15,18,'pig','2016-12-07 06:56:58','superman',0),(16,18,'apple','2016-12-07 06:57:05','superman',0),(17,11,'meow','2016-12-07 07:53:50','superman',0),(18,16,'magic','2016-12-07 07:54:56','superman',0);
/*!40000 ALTER TABLE `Comment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Employee`
--

DROP TABLE IF EXISTS `Employee`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Employee` (
  `SSN` int(11) NOT NULL DEFAULT '0',
  `UserID` varchar(20) DEFAULT NULL,
  `StartDate` date DEFAULT NULL,
  `HourlyRate` double DEFAULT NULL,
  PRIMARY KEY (`SSN`),
  UNIQUE KEY `UserID` (`UserID`),
  CONSTRAINT `Employee_ibfk_1` FOREIGN KEY (`UserID`) REFERENCES `User` (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Employee`
--

LOCK TABLES `Employee` WRITE;
/*!40000 ALTER TABLE `Employee` DISABLE KEYS */;
INSERT INTO `Employee` VALUES (123211111,'5','2016-11-07',10),(123456789,'4','2016-11-07',10),(130558963,'12','2016-11-07',10),(222223333,'7','2016-11-07',10),(412663985,'10','2016-11-07',10),(432558565,'13','2016-11-07',10),(554463217,'8','2016-11-07',10),(774856332,'11','2016-11-07',10),(852146391,'9','2016-11-07',10),(987654321,'6','2016-11-07',10);
/*!40000 ALTER TABLE `Employee` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Friendship`
--

DROP TABLE IF EXISTS `Friendship`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Friendship` (
  `UserId1` varchar(20) DEFAULT NULL,
  `UserId2` varchar(20) DEFAULT NULL,
  KEY `UserId1` (`UserId1`),
  KEY `UserId2` (`UserId2`),
  CONSTRAINT `Friendship_ibfk_1` FOREIGN KEY (`UserId1`) REFERENCES `User` (`UserID`),
  CONSTRAINT `Friendship_ibfk_2` FOREIGN KEY (`UserId2`) REFERENCES `User` (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Friendship`
--

LOCK TABLES `Friendship` WRITE;
/*!40000 ALTER TABLE `Friendship` DISABLE KEYS */;
INSERT INTO `Friendship` VALUES ('superman','1'),('1','2'),('1','3'),('1','4'),('2','3'),('2','4'),('3','4'),('7','8'),('6','5'),('9','10'),('10','3');
/*!40000 ALTER TABLE `Friendship` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `GroupComment`
--

DROP TABLE IF EXISTS `GroupComment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `GroupComment` (
  `CommentID` int(11) NOT NULL AUTO_INCREMENT,
  `PostID` int(11) DEFAULT NULL,
  `Content` char(140) DEFAULT NULL,
  `CDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `AuthorID` varchar(20) DEFAULT NULL,
  `LikeCounter` int(11) DEFAULT NULL,
  PRIMARY KEY (`CommentID`),
  KEY `AuthorID` (`AuthorID`),
  KEY `PostID` (`PostID`),
  CONSTRAINT `GroupComment_ibfk_1` FOREIGN KEY (`AuthorID`) REFERENCES `User` (`UserID`),
  CONSTRAINT `GroupComment_ibfk_2` FOREIGN KEY (`PostID`) REFERENCES `GroupPost` (`PostID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `GroupComment`
--

LOCK TABLES `GroupComment` WRITE;
/*!40000 ALTER TABLE `GroupComment` DISABLE KEYS */;
/*!40000 ALTER TABLE `GroupComment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `GroupPost`
--

DROP TABLE IF EXISTS `GroupPost`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `GroupPost` (
  `PostID` int(11) NOT NULL AUTO_INCREMENT,
  `AuthorID` varchar(20) DEFAULT NULL,
  `GroupID` int(11) DEFAULT NULL,
  `PDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `Content` varchar(255) DEFAULT NULL,
  `CommentCount` int(11) DEFAULT NULL,
  `LikeCounter` int(11) DEFAULT NULL,
  PRIMARY KEY (`PostID`),
  KEY `AuthorID` (`AuthorID`),
  KEY `GroupID` (`GroupID`),
  CONSTRAINT `GroupPost_ibfk_1` FOREIGN KEY (`AuthorID`) REFERENCES `User` (`UserID`),
  CONSTRAINT `GroupPost_ibfk_2` FOREIGN KEY (`GroupID`) REFERENCES `Groups` (`GroupID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `GroupPost`
--

LOCK TABLES `GroupPost` WRITE;
/*!40000 ALTER TABLE `GroupPost` DISABLE KEYS */;
/*!40000 ALTER TABLE `GroupPost` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Groups`
--

DROP TABLE IF EXISTS `Groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Groups` (
  `GroupID` int(11) NOT NULL AUTO_INCREMENT,
  `Owner` varchar(20) DEFAULT NULL,
  `GroupName` char(20) DEFAULT NULL,
  `Type` char(20) DEFAULT NULL,
  PRIMARY KEY (`GroupID`),
  KEY `Owner` (`Owner`),
  CONSTRAINT `Groups_ibfk_1` FOREIGN KEY (`Owner`) REFERENCES `User` (`UserID`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Groups`
--

LOCK TABLES `Groups` WRITE;
/*!40000 ALTER TABLE `Groups` DISABLE KEYS */;
INSERT INTO `Groups` VALUES (1,'1','cheese Club','Club'),(2,'1','Math Club','Organization'),(3,'2','English Club','Work'),(4,'2','Sience Club','Personal'),(5,'3','game Club','Personal'),(6,'3','history Club','Organization'),(7,'4','random Club','Club'),(8,'7','super club','Club'),(9,'5','asdcw Club','Organization'),(10,'8','animate Club','Work'),(11,'superman','testfunction','Organization');
/*!40000 ALTER TABLE `Groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Message`
--

DROP TABLE IF EXISTS `Message`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Message` (
  `MessageID` int(11) NOT NULL AUTO_INCREMENT,
  `SendDate` date DEFAULT NULL,
  `SenderID` varchar(20) DEFAULT NULL,
  `ReceiverID` varchar(20) DEFAULT NULL,
  `Subject` char(40) DEFAULT NULL,
  `Content` char(140) DEFAULT NULL,
  PRIMARY KEY (`MessageID`),
  KEY `SenderID` (`SenderID`),
  KEY `ReceiverID` (`ReceiverID`),
  CONSTRAINT `Message_ibfk_1` FOREIGN KEY (`SenderID`) REFERENCES `User` (`UserID`),
  CONSTRAINT `Message_ibfk_2` FOREIGN KEY (`ReceiverID`) REFERENCES `User` (`UserID`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Message`
--

LOCK TABLES `Message` WRITE;
/*!40000 ALTER TABLE `Message` DISABLE KEYS */;
INSERT INTO `Message` VALUES (1,'2016-11-07','1','2','how are you','do you wanna do work together?'),(2,'2016-11-08','2','1','how are you2','yes?'),(3,'2016-11-09','1','3','how are you3','do you wanna do work together?'),(4,'2016-11-10','3','2','how are you4','where are you?'),(5,'2016-11-01','1','4','how are you5','do you wanna do work together?'),(6,'2016-11-02','4','3','how are you6','what do you wanna eat?'),(7,'2016-11-03','2','4','how are you7','hi?'),(8,'2016-11-04','1','2','how are you8','good?'),(9,'2016-11-13','4','8','how are asdc','asdasdw?'),(10,'2016-11-22','9','13','how are no idea','bfbf?');
/*!40000 ALTER TABLE `Message` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `PartOf`
--

DROP TABLE IF EXISTS `PartOf`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PartOf` (
  `UserID` varchar(20) DEFAULT NULL,
  `GroupID` int(11) DEFAULT NULL,
  KEY `UserID` (`UserID`),
  KEY `GroupID` (`GroupID`),
  CONSTRAINT `PartOf_ibfk_1` FOREIGN KEY (`UserID`) REFERENCES `User` (`UserID`),
  CONSTRAINT `PartOf_ibfk_2` FOREIGN KEY (`GroupID`) REFERENCES `Groups` (`GroupID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `PartOf`
--

LOCK TABLES `PartOf` WRITE;
/*!40000 ALTER TABLE `PartOf` DISABLE KEYS */;
INSERT INTO `PartOf` VALUES ('2',1),('3',1),('4',1),('5',2),('6',2),('6',2),('7',3),('8',3),('9',3),('10',4),('2',4),('superman',11);
/*!40000 ALTER TABLE `PartOf` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Post`
--

DROP TABLE IF EXISTS `Post`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Post` (
  `PostID` int(11) NOT NULL AUTO_INCREMENT,
  `AuthorID` varchar(20) DEFAULT NULL,
  `TargetID` varchar(20) DEFAULT NULL,
  `PDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `Content` char(140) DEFAULT NULL,
  `CommentCount` int(11) DEFAULT NULL,
  `LikeCounter` int(11) DEFAULT NULL,
  PRIMARY KEY (`PostID`),
  KEY `AuthorID` (`AuthorID`),
  KEY `TargetID` (`TargetID`),
  CONSTRAINT `Post_ibfk_1` FOREIGN KEY (`AuthorID`) REFERENCES `User` (`UserID`),
  CONSTRAINT `Post_ibfk_2` FOREIGN KEY (`TargetID`) REFERENCES `User` (`UserID`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Post`
--

LOCK TABLES `Post` WRITE;
/*!40000 ALTER TABLE `Post` DISABLE KEYS */;
INSERT INTO `Post` VALUES (11,'superman','superman','2016-12-07 05:05:38','123',0,2),(12,'superman','superman','2016-12-07 05:06:07','444',0,0),(13,'superman','superman','2016-12-07 05:08:49','chicken',0,0),(14,'1','1','2016-12-07 05:25:29','how are you',0,0),(15,'2','3','2016-12-07 05:26:15','hasdasdow are you',0,0),(16,'superman','superman','2016-12-07 05:58:44','test post',0,0),(17,'superman','superman','2016-12-07 05:59:57',':)',0,3),(18,'1','1','2016-12-07 06:05:22',':D',0,2);
/*!40000 ALTER TABLE `Post` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Sale`
--

DROP TABLE IF EXISTS `Sale`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Sale` (
  `TransactionID` int(11) NOT NULL auto_increment, 
  `AccountNumber` int(11) DEFAULT NULL,
  `AdvertisementID` int(11) DEFAULT NULL,
  `SaleTime` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  `Qty` int(11) DEFAULT NULL,
  PRIMARY KEY (`TransactionID`),
  KEY `AccountNumber` (`AccountNumber`),
  KEY `AdvertisementID` (`AdvertisementID`),
  CONSTRAINT `Sale_ibfk_1` FOREIGN KEY (`AccountNumber`) REFERENCES `Account` (`AccountNumber`),
  CONSTRAINT `Sale_ibfk_2` FOREIGN KEY (`AdvertisementID`) REFERENCES `Advertisement` (`AdvertisementID`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Sale`
--

LOCK TABLES `Sale` WRITE;
/*!40000 ALTER TABLE `Sale` DISABLE KEYS */;
INSERT INTO `Sale` VALUES (1,1,1,'2014-09-23 08:22:01',2),(2,2,1,'2014-09-23 08:22:01',2),(3,3,2,'2014-09-23 08:22:01',2),(4,4,2,'2014-09-23 08:22:01',2),(5,5,1,'2014-09-23 08:22:01',2),(6,6,4,'2014-09-24 08:22:01',1),(7,7,5,'2014-09-25 08:22:01',2),(8,8,6,'2014-09-26 08:22:01',3),(9,9,7,'2014-09-27 08:22:01',4),(10,10,8,'2014-09-28 08:22:01',5);
/*!40000 ALTER TABLE `Sale` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `User`
--

DROP TABLE IF EXISTS `User`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `User` (
  `UserID` varchar(20) NOT NULL,
  `Password` varchar(11) NOT NULL,
  `FirstName` char(20) DEFAULT NULL,
  `LastName` char(20) DEFAULT NULL,
  `EmailAddress` char(30) DEFAULT NULL,
  `Preferences` varchar(255) DEFAULT NULL,
  `City` char(20) DEFAULT NULL,
  `State` char(20) DEFAULT NULL,
  `Address` char(50) DEFAULT NULL,
  `ZipCode` int(11) DEFAULT NULL,
  `Telephone` bigint(20) DEFAULT NULL,
  `CreditCardNumber` bigint(20) DEFAULT NULL,
  `Rating` char(15) DEFAULT NULL,
  PRIMARY KEY (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `User`
--

LOCK TABLES `User` WRITE;
/*!40000 ALTER TABLE `User` DISABLE KEYS */;
INSERT INTO `User` VALUES ('1','123','Rofl','Mompol','gg@yahoo.com',NULL,'East Meadow','New York','Lmao Lane',11380,323131311,432232421,'Active'),('10','123','Snowflake','Bryant','sBryant@gmail.com','Amps','New York City','New York','i live here street',12345,55955555,1233341234,'Very Active'),('11','123','ff','scx','sBwdcsant@gmail.com','ver','New York City','New York','i live here street',8226,223312,1233341234,'Very Active'),('12','123','jhon','Mike','mjon@gmail.com','game','somewhere','tokyo','i live here street',32651,8845550222,15251,'Very Active'),('13','123','Kim','SINL','as?sd@gmail.com','wdw','stony brook','New York','i live here street',51521,175213885,2232515566,'Very Active'),('2','123','La','Lamo','lamo@yahoo.com',NULL,'Westbury','New York','Elda Lane',11380,456743215,432232422,'Active'),('3','123','Somo','Sommmmmmm','gg@okokok.com',NULL,'Valley Stream','New York','Laf Lane',11567,689898983,432232422,'Active'),('4','123','Ads','Adddy','Popop@yahoo.com',NULL,'Astoria','New York','Popo Lane',12314,345678345,432232424,'Active'),('5','123','employee','asd','chicken@yahoo.com',NULL,'stony brook','New York','22-22',45119,78955387,465891282,'Active'),('6','123','Uzi','Vert','emai333@gmail.com','be','Detroit','Michigan','i live here street',12325,11111111,1234125034,'Very Active'),('7','123','Jidenna','The Man','em32il1@gmail.com','honst','Omaha','Nebraska','i live here street',11345,12355551,1127641234,'Very Active'),('8','123','Jermaine','Cole','ema5572@gmail.com','so','Pheonix','Arizona','i live here street',12365,55559555,1234112234,'Very Active'),('9','123','Kanye','West','em1@gmail.com','whatever','New York City','New York','i live here street',10345,55857555,1234341234,'Very Active'),('superman','1','1','1','1@1','1','1','1','1',1,1,1,'Normal');
/*!40000 ALTER TABLE `User` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-12-07  2:59:48
