 var app = angular.module("managerApp", ['ngRoute', 'ngAnimate', 'ngSanitize', 'ui.bootstrap']);
app.config(["$routeProvider", "$locationProvider",function($routeProvider, $locationProvider) {
          $routeProvider
          .when("/", {
              templateUrl : "manageEmp.htm"
          })
          .when("/saleRep", {
              templateUrl : "saleRep.htm"
          })
          .when("/adList", {
              templateUrl : "adList.htm"
          })
          .when("/tList", {
              templateUrl : "tList.htm"
          })
          .when("/saleSum", {
              templateUrl : "saleSum.htm"
          })
          .when("/revGen", {
              templateUrl : "revGen.htm"
          })
          .when("/ciList", {
               templateUrl : "ciList.htm"
          });
      }]);

app.controller('mngrCtrl',function($scope,$location,$http, $uibModal){


  $scope.getAdInfo =function(size){
    adListModal = $uibModal.open({
      templateUrl: 'adList.html',
      controller: 'adListCtrl'
    })
  }

  $scope.getCompanyAdInfo = function(size){
    companyAdModal = $uibModal.open({
      templateUrl: 'companyAdList.html',
      controller: 'companyAdCtrl'
    })
  }

  $scope.getCustByItem = function(size){
    custItemModal = $uibModal.open({
      templateUrl: 'custItemList.html',
      controller: 'custItemCtrl'
    })
  }

  $scope.mostValCust = function(size){
    mvCustModal = $uibModal.open({
      templateUrl: 'mvCust.html',
      controller: 'mvCustCtrl'
    })
  }

  $scope.mostValEmp = function(size){
    mvEmpModal = $uibModal.open({
      templateUrl: 'mvEmp.html',
      controller: 'mvEmpCtrl'
    })
  }
  $scope.getEmpInfo = function(size) {
    empModal = $uibModal.open({
      templateUrl: 'manageEmp.html',
      controller: 'empCtrl',
    })
  }

  $scope.getSalesReport = function(size){
    salesReportModal = $uibModal.open({
      templateUrl: 'salesReportMonth.html',
      controller: 'salesReportCtrl'
    })
  }

    $scope.getSale = function(size) {
    empModal = $uibModal.open({
      templateUrl: 'getSale.html',
      controller: 'getSaleCtrl',
    })
  }

  $scope.getSalesItemUser = function(size){
    salesModal = $uibModal.open({
      templateUrl: 'salesByOptions.html',
      controller: 'salesListCtrl'
    })
  }

});

app.controller('addCtrl', function($scope, srvShareData, $location, $http){
  $scope.sub = function(){ 
    $http.post('/signupEmp', $scope.signUp).
    success(function(res){
      if(res =="error")
        $scope.info = "Error while creating User"
      else if(res =="3"){
        $scope.info = "UserID already exist!"
      }
      else if(res =="1"){
        $scope.info ="Success!";
        var dataToShare = {UserID: $scope.signUp.UserID};
        srvShareData.addData(dataToShare);
        addempModal.dismiss('cancel')
      }

    })
  }

$scope.closeaddemp = function(){
    addempModal.dismiss('cancel')
  }

});

 app.controller('salesListCtrl',function($scope,$location,$http,$uibModal){
  $scope.getSalesByItem = function(){
    var searchItem = $scope.searchItem
    var post = {
      searchItem: searchItem
    }
    $http.post('/getSalesByItem', post).success(function(res){
        $scope.items = res;
    })
  }
$scope.getSalesByName = function(){
  var searchItem = $scope.searchName;
  var res = searchItem.split(" ");
  var post = {
    FirstName:res[0],
    LastName:res[1]
  }

  $http.post('/getSalesByName', post).success(function(res){
    $scope.items = res;
  })
}

  $scope.close = function(){
    salesModal.dismiss('cancel')
  }
 })

 app.controller('salesReportCtrl',function($scope,$location,$http,$uibModal){
    $scope.getSalesReport = function(){
      var month = $scope.startMonth
      var year = $scope.startYear
      var post = {
        month: month,
        year: year
      }
      $http.post('/getSalesReport',post).success(function(res){
        $scope.sales = res;
 
      })

    }



    $scope.close = function(){
      salesReportModal.dismiss('cancel')
    }
 })

app.controller('mvEmpCtrl',function($scope,$location,$http,$uibModal){

  $http.post('/getMvEmp').success(function(res){
      $scope.mvEmpName = res[0];
  })



   $scope.close = function(){
    mvEmpModal.dismiss('cancel')
  }
})

app.controller('mvCustCtrl',function($scope,$location,$http,$uibModal){
  $http.post('/getMvCust').success(function(res){
      $scope.mvCustName = res[0];
  })

     $scope.close = function(){
  mvCustModal.dismiss('cancel')
 }
})

app.controller('getSaleCtrl', function($scope,$location,$http, $uibModal){
  $scope.hideItem = true;
  $scope.hideType = true;
  $scope.hideCustomer = true;

  $scope.show = function(index){
    if(index ==1){
      $scope.hideItem = false;
      $scope.hideType = true;
      $scope.hideCustomer = true;

    }
    else if(index == 2){
            $scope.hideItem = true;
      $scope.hideType = false;
      $scope.hideCustomer = true;
    }
    else if(index ==3 ){
            $scope.hideItem = true;
      $scope.hideType = true;
      $scope.hideCustomer = false;
    }
  }

  $scope.getByItem = function(){
    var post = {
      AdvertisementID:$scope.AdvertisementID
    }
    $http.post('/getByItem', post).
    success(function(res){
      $scope.items = res
    })
  }

    $scope.getByType = function(){
    var post = {
      Type:$scope.Type
    }
    $http.post('/getByType', post).
    success(function(res){
      $scope.items = res
    })
  }

  $scope.getByAcc = function(){
    var post = {
      AccountNumber: $scope.AccountNumber
    }
        $http.post('/getByAcc', post).
    success(function(res){
      $scope.items = res
    })
  }

       $scope.close = function(){
  empModal.dismiss('cancel')
 }

})

app.controller('custItemCtrl',function($scope,$location,$http, $uibModal){


$scope.getCustItem = function(){
  var post = {
    itemName : $scope.itemName
  }
  $http.post('/getAllItemsByCust', post).success(function(res){
    $scope.companyAds = res;
  })
}
   $scope.close = function(){
  custItemModal.dismiss('cancel')
 }

})

app.controller('adListCtrl',function($scope,$location,$http,$uibModal){

  $http.post('/getAllAds').
  success(function(res){
  $scope.ads = res;
  });


 $scope.close = function(){
  adListModal.dismiss('cancel')
 }
});

app.controller('companyAdCtrl',function($scope,$location,$http,$uibModal){

  $scope.getCompanyAds = function(){
    var post = {
    Company: $scope.companyName
  }

  $http.post('/getCompanyAds',post).
  success(function(res){
  $scope.companyAds = res;
  });
}


 $scope.close = function(){
  companyAdModal.dismiss('cancel')
 }
});

app.controller('empCtrl',function($scope,$location,$http,$uibModal,srvShareData) {

    $http.post('/getAllEmp').
    success(function(res) {
      $scope.emps = res;
    });

$scope.getEmpInfo = function(size) {
    empModal = $uibModal.open({
      templateUrl: 'manageEmp.html',
      controller: 'empCtrl',
    })
  }

  $scope.deleteEmp = function(emp) {
    $http.post('/deleteEmp',emp).
    success(function(res) {
      $http.post('/getAllEmp').
    success(function(res) {
      $scope.emps = res;
    });
    })

  }

  $scope.closeemp = function() {
    empModal.dismiss('cancel')
  }

  $scope.addEmp = function() {
    empModal.dismiss('cancel')
    addempModal = $uibModal.open({
      templateUrl: 'addEmp.html',
      controller: 'addCtrl',
    })
  }

  $scope.editEmp = function(size,emp) {
    empModal.dismiss('cancel')
    var dataToShare = {SSN: emp.SSN}
    srvShareData.addData(dataToShare)
    editempModal = $uibModal.open({
      templateUrl: 'editEmp.html',
      controller: 'editCtrl',
    })
  }

});

app.controller('editCtrl', function($scope, srvShareData, $location, $http){
  
  $scope.sub = function(){ 
    $http.post('/editEmp', $scope.signUp).
    success(function(res){
      if(res =="error")
        $scope.info = "Error while creating User"
      else if(res =="3"){
        $scope.info = "UserID already exist!"
      }
      else if (res == "2") {
        $scope.info = "employee was not created ssn is null"
      }
      else if(res =="1"){
        $scope.info ="Success!";
        var dataToShare = {UserID: $scope.signUp.UserID};
        srvShareData.addData(dataToShare);
        editempModal.dismiss('cancel')
      }

    })
  }

  var getData = srvShareData.getData()


  $http.post('/getEmpData', getData).
    success(function(res) {
      $scope.signUp = {UserID: res[0].UserID, Password: res[0].Password, FirstName: res[0].FirstName, LastName: res[0].LastName, Email: res[0].EmailAddress, Preferences: res[0].Preferences, City: res[0].City, State: res[0].State, Address: res[0].Address, ZipCode: res[0].ZipCode, Telephone: res[0].Telephone, CreditCard: res[0].CreditCardNumber, EmpSSN: res[0].SSN, StartDate: Date(res[0].StartDate), hourlyRate: res[0].HourlyRate}
    });

$scope.closeeditemp = function(){
    editempModal.dismiss('cancel')
  }

});

app.service('srvShareData', function($window) {
        var KEY = 'App.SelectedValue';

        var addData = function(newObj) {
            var mydata = $window.sessionStorage.getItem(KEY);
            if (mydata) {
                mydata = JSON.parse(mydata);
            } else {
                mydata = [];
            }
            mydata[0] = newObj;
            $window.sessionStorage.setItem(KEY, JSON.stringify(mydata));
        };
        var addGroup = function(newObj) {
            var mydata = $window.sessionStorage.getItem(KEY);
            if (mydata) {
                mydata = JSON.parse(mydata);
            } else {
                mydata = [];
            }
            mydata[1] = newObj;
            $window.sessionStorage.setItem(KEY, JSON.stringify(mydata));
        };
        var addFriend = function(newObj) {
            var mydata = $window.sessionStorage.getItem(KEY);
            if (mydata) {
                mydata = JSON.parse(mydata);
            } else {
                mydata = [];
            }
            mydata[2] = newObj;
            $window.sessionStorage.setItem(KEY, JSON.stringify(mydata));
        };

        var getData = function(){
            var mydata = $window.sessionStorage.getItem(KEY);
            if (mydata) {
                mydata = JSON.parse(mydata);
            }
            return mydata || [];
        };

        var editPost = function(newObj){
           var mydata = $window.sessionStorage.getItem(KEY);
            if (mydata) {
                mydata = JSON.parse(mydata);
            } else {
                mydata = [];
            }
            mydata[3] = newObj;
            $window.sessionStorage.setItem(KEY, JSON.stringify(mydata));
        };     
                var editComment = function(newObj){
           var mydata = $window.sessionStorage.getItem(KEY);
            if (mydata) {
                mydata = JSON.parse(mydata);
            } else {
                mydata = [];
            }
            mydata[4] = newObj;
            $window.sessionStorage.setItem(KEY, JSON.stringify(mydata));
        };    
        return {
          addGroup: addGroup,
            addData: addData,
            getData: getData,
            addFriend: addFriend,
            editPost: editPost,
            editComment: editComment
        };
    });


