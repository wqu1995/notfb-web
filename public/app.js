var app = angular.module('notfb', ['ngRoute','ngAnimate', 'ngSanitize', 'ui.bootstrap']);
var createGroupModal;
app.config(["$routeProvider", "$locationProvider", function($routeProvider, $locationProvider){
    $routeProvider
		.when("/", {
			templateUrl: "index.html",
			controller: "signInCtrl"
		})
		.when("/mainPage", {
			templateUrl: "mainPage.html",
			controller: "mainCtrl"
		})
    .when("/group",{
      templateUrl: "group.html",
      controller: "groupPageCtrl"
    })
    .when("/signUp",{
      templateUrl: "signUp.html",
      controller: "signUpCtrl"
    })
    .when("/friend",{
      templateUrl: "friend.html",
      controller: "friendPageCtrl"
    })
    .when("/managerPage",{
      templateUrl: "managerPage.html",
      controller: "mngrCtrl"
    })
		// .otherwise({ redirectTo: '/'})
		;
}]);

app.controller('friendPageCtrl', function ($uibModal, $document, $scope, srvShareData, $location, $http){
    var info = srvShareData.getData();
  $scope.FirstName = info[2].FirstName;
  $scope.LastName = info[2].LastName;

    var getPostData = function(){
    var postDataA = [];
    var anotherData;
    $http.post('/getAllFriendPosts', info[2]).
    success(function(res){
      var  postData = res;
    angular.forEach(postData, function(value,key){
      $http.post('/getPersonalComment', value).
      success(function(res){
        var comment = {Comments: res};
        var createData = angular.extend(value, comment);
        postDataA.push(createData);
        $scope.posts = postDataA;
      })

    })
    //$scope.posts = res;

  })
}


  getPostData();

  $http.post('/checkOwner', info).
  success(function(res){
    if(res == "1")
      $scope.mgnHide=false;
    else if(res == "3")
      $scope.mgnHide=true;
    else
      $scope.mgnHide=true;

  });
  $scope.deletePost = function(post){
    $http.post('/deletePost', post).
    success(function(res){
      if(res =="error")
        alert("Delete post failed");
      else
        getPostData();
    })
  }
  $scope.editPost = function(item,size){
      srvShareData.editPost(item)
      editPostModal = $uibModal.open({
      templateUrl: 'editPost.html',
      controller:'editPostCtrl',
      size: size
    })
      
  }
  $scope.checkOwnerComment = function(comment){
    if (comment.AuthorID == info[0].UserID){
      return true;
    }
    else{
      return false;
    }
  }

  $scope.deleteComment = function(comment){
    $http.post('/deletecomment', comment).
    success(function(res){
      if(res =="error")
        alert("Delete post failed");
      else
        getPostData();
    })
  }
  $scope.editComment = function(item,size){
      srvShareData.editComment(item)
      editPostModal = $uibModal.open({
      templateUrl: 'editComment.html',
      controller:'editCommentCtrl',
      size: size
    })
      
  }
  $scope.checkOwnerPost = function(post){
    if (post.AuthorID == info[0].UserID){
      return true;
    }
    else{
      return false;
    }
  }
   $scope.likePost = function(post){
    var postID = post.PostID;
    var counter = '+1';
    var reqToSend = {
        postID,
        counter
    };
   $http.post('/updatePostLike', reqToSend).success(function(res){
      if(res == "success"){
        getPostData();
      }
      else{
        alert("Liking Post failed")
      }
    })
  };

  $scope.likeComment = function(comment){
    var commentID = comment.CommentID;
    var counter = '+1';
    var reqToSend = {
      commentID,
      counter
    };
    $http.post('/updateCommentLike', reqToSend).
    success(function(res){
      if(res == "1"){
        getPostData();
      }
      else{
        alert("Likeing comment failed")
      }
    })

  };
    $scope.unlikeComment = function(comment){
    var commentID = comment.CommentID;
    var counter = '-1';
    var reqToSend = {
      commentID,
      counter
    };
    $http.post('/updateCommentLike', reqToSend).
    success(function(res){
      if(res == "1"){
        getPostData();
      }
      else{
        alert("Likeing comment failed")
      }
    })

  };

 $scope.unlike = function(post){
    var postID = post.PostID;
    var counter = '-1';
    var reqToSend = {
        postID,
        counter
    };
   $http.post('/updatePostLike', reqToSend).success(function(res){
      if(res == "success"){
        getPostData();
      }
      else{
        alert("Liking Post failed")
      }
    })
  }

  $scope.makeFriendPost = function(){
    

    var postToBeMade = $scope.postToMake;
    var jsonPost = {
      UserID: info[0].UserID,
      TargetID: info[2].UserID,
      Content: postToBeMade
    };

    $http.post('/makeFriendPost', jsonPost).success(function(res){
      if (res == "error"){
        $scope.postInfo = "An error occurred"
      }
      else if(res =="1"){
        $scope.postInfo = "You have made a post"
        getPostData();
      }
    });

  }

    $scope.makeComment = function(post, comm){
    var postID = post.PostID;
    var UserID = info[0].UserID;
    var jsonPost = {
      UserID,
      comm,
      postID
    }
    $http.post('/makeComment', jsonPost).success(function(res){
      if (res == "success"){
        getPostData();
      }
      else{
        alert("Comment failed")
      }
    })
  }
})

app.controller('signUpCtrl', function($scope, srvShareData, $location, $http){
  $scope.sub = function(){ 
    $http.post('/signup', $scope.signUp).
    success(function(res){
      if(res =="error")
        $scope.info = "Error while creating User"
      else if(res =="3"){
        $scope.info = "UserID already exist!"
      }
      else if(res =="1"){
        $scope.info ="Success!";
        var dataToShare = {UserID: $scope.signUp.UserID};
        srvShareData.addData(dataToShare);
       window.location.href = "mainPage.html";
      }

    })
  }
});

app.controller('signInCtrl', function($scope, srvShareData, $location, $http){
	$scope.dataToShare=[];

	$scope.sub = function(){
		$http.post('/signIn', $scope.signIn).
		success(function(res){
			if(res ==="error"){
				$scope.loginInfo = "An error occurred!";
			}
      else if(res ==="3")
        $scope.loginInfo = "Wrong UserID and/or Password!";
			else{
				$scope.dataToShare = res[0];

				srvShareData.addData($scope.dataToShare);
				window.location.href = "mainPage.html";
			}

		})
	}
});

app.controller('signInMngrCtrl', function($scope, srvShareData, $location, $http){
  $scope.dataToShare=[];

  $scope.sub = function(){
    if ($scope.UserID == 'krew'){
      if($scope.Password == '123'){
        window.location.href = "/mgn/managerPage.html";
      }
    }
  }
    /*
    $http.post('/signIn', $scope.signIn).
    success(function(res){
      if(res ==="error"){
        $scope.loginInfo = "An error occurred!";
      }
      else if(res ==="3")
        $scope.loginInfo = "Wrong UserID and/or Password!";
      else{
        $scope.dataToShare = res[0];

        srvShareData.addData($scope.dataToShare);
        
      }

    })
  }*/
});

app.controller('navCtrl',function($scope,srvShareData,$location,$http, $uibModal){
  $scope.logOut = function(){
    window.location.href = "index.html";
  }
  $scope.home = function(){
    window.location.href = "mainPage.html";
  }
})

app.controller('FriendCtrl', function($scope,srvShareData,$location,$http, $uibModal){
  var userInfo = srvShareData.getData();

  $scope.retriveData = function(){
  }

  $scope.addFriend = function(size, parentSelector){
      var parentElem = parentSelector ? 
      angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
      createGroupModal = $uibModal.open({
        animation: $scope.animationsEnabled,
        ariaLabelledBy: 'modal-title',
        ariaDescribedBy: 'modal-body',
        templateUrl: 'AddFriend.html',
        controller: 'FriendCtrl',

        size: size,
        appendTo: parentElem,
      });
    }

  $scope.subAddFriend = function(){
    var jsonPost = {
      UserID: userInfo[0].UserID,
      friendID: $scope.UserID
    }
    $http.post('/addFriend', jsonPost).
    success(function(res){
      if(res == "error")
        $scope.info = "Error while adding friend";
      else if(res == "1"){
        $scope.info = "You have added the friend, you can close the window now";
      }

      else if(res == "3")
        $scope.info = "User is already your friend!"
    })
    
  }

    $scope.deleteFriend = function(size, parentSelector){
      var parentElem = parentSelector ? 
      angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
      createGroupModal = $uibModal.open({
        animation: $scope.animationsEnabled,
        ariaLabelledBy: 'modal-title',
        ariaDescribedBy: 'modal-body',
        templateUrl: 'deleteFriend.html',
        controller: 'FriendCtrl',

        size: size,
        appendTo: parentElem,
  
      });
    }

  $scope.subDeleteFriend = function(){
    var jsonPost = {
      UserID: userInfo[0].UserID,
      friendID: $scope.UserID
    }
    $http.post('/deleteFriend', jsonPost).
    success(function(res){
      if(res == "error")
        $scope.info = "Error while adding friend";
      else if(res == "1")
        $scope.info = "You have delete the friend, you can close the window now"
      else if(res == "3")
        $scope.info = "User is not your friend!"
    })
    
  }

  $scope.retriveFriends = function(){
    $http.post('/friends', userInfo[0]).
    success(function(res){
    $scope.friends = res;
    });
  }
  

  $scope.goToFriend = function(index){
      $scope.groupInfo=[];
      $scope.groupInfo.push($scope.friends[index].UserID);
      //var test = {GroupID: .GroupID};

      
      srvShareData.addFriend($scope.friends[index]);
      var temp = srvShareData.getData();
      window.location.href = "friend.html";
    }

  $scope.close= function(){
    createGroupModal.dismiss('cancel');
  }

    /*$scope.addFriend = function(){
    var friendsEmail = $scope.friendsEmail
    var emailsToSend = {
      UserID,
      friendsEmail
    };
    $http.post('/addFriend', emailsToSend).success(function(res){
      if(res == "success"){
        alert("Friend was added");
      }
      else{
        alert("Friend does not exist");
      }
    })  
  }*/

})

app.controller('mainCtrl',function($scope,srvShareData,$location,$http, $uibModal){
	var userInfo = srvShareData.getData();

	var UserID =userInfo[0].UserID;
  var Email = userInfo[0].Email;
  $scope.userID = UserID;
  $http.post('/', userInfo[0]).
  success(function(res){
    $scope.Name = res[0].FirstName +" "+ res[0].LastName;
  });
 
   
  var getPostData = function(){

    var postDataA = [];
    var anotherData;
    $http.post('/getAllFriendPosts', userInfo[0]).
    success(function(res){
      var  postData = res;
    angular.forEach(postData, function(value,key){
      $http.post('/getPersonalComment', value).
      success(function(res){
        var comment = {Comments: res};
        var createData = angular.extend(value, comment);
        postDataA.push(createData);
        $scope.posts = postDataA;
      })

    })
    //$scope.posts = res;

  })
}
//retrieves employee data
/*
var empDataA;
  var getEmployeeData = function(){
    $http.post('/getAllEmp', null).
    success(function(res) {
      empDataA = res;
      $scope.emps = empDataA;
    })
  }
getEmployeeData();*/
getPostData();




 $scope.likePost = function(post){
    var postID = post.PostID;
    var counter = '+1';
    var reqToSend = {
        postID,
        UserID,
        counter
    };
   $http.post('/updatePostLike', reqToSend).success(function(res){
      if(res == "success"){
        getPostData();
      }
      else{
        alert("Liking Post failed")
      }
    })
  };

  $scope.likeComment = function(comment){
    var commentID = comment.CommentID;
    var counter = '+1';
    var reqToSend = {
      commentID,
      counter
    };
    $http.post('/updateCommentLike', reqToSend).
    success(function(res){
      if(res == "1"){
        getPostData();
      }
      else{
        alert("Likeing comment failed")
      }
    })

  };
    $scope.unlikeComment = function(comment){
    var commentID = comment.CommentID;
    var counter = '-1';
    var reqToSend = {
      commentID,
      counter
    };
    $http.post('/updateCommentLike', reqToSend).
    success(function(res){
      if(res == "1"){
        getPostData();
      }
      else{
        alert("Likeing comment failed")
      }
    })

  };

 $scope.unlike = function(post){
    var postID = post.PostID;
    var counter = '-1';
    var reqToSend = {
        postID,
        UserID,
        counter
    };
   $http.post('/updatePostLike', reqToSend).success(function(res){
      if(res == "success"){
        getPostData();
      }
      else{
        alert("Liking Post failed")
      }
    })
  }

  $scope.makePost = function(){
    var sharedData = srvShareData.getData();

    var postToBeMade = $scope.postToMake;
    var jsonPost = {
      UserID: sharedData[0].UserID,
      Content: postToBeMade
    };

    $http.post('/makePersonalPost', jsonPost).success(function(res){
      if (res == "error"){
        $scope.postInfo = "An error occurred"
      }
      else if(res =="1"){
        $scope.postInfo = "You have made a post"
        getPostData();
      }
    });

  }

  $scope.editPost = function(item,size){
      srvShareData.editPost(item)
      editPostModal = $uibModal.open({
      templateUrl: 'editPost.html',
      controller:'editPostCtrl',
      size: size
    })
      
  }
  $scope.editComment = function(item,size){
      srvShareData.editComment(item)
      editPostModal = $uibModal.open({
      templateUrl: 'editComment.html',
      controller:'editCommentCtrl',
      size: size
    })
      
  }

  $scope.deletePost = function(post){
    $http.post('/deletePost', post).
    success(function(res){
      if(res =="error")
        alert("Delete post failed");
      else
        getPostData();
    })
  }
  $scope.deleteComment = function(comment){
    $http.post('/deletecomment', comment).
    success(function(res){
      if(res =="error")
        alert("Delete post failed");
      else
        getPostData();
    })
  }

  $scope.checkOwnerPost = function(post){
    if (post.AuthorID == UserID){
      return true;
    }
    else{
      return false;
    }
  }
  $scope.checkOwnerPost = function(post){
    if (post.AuthorID == UserID){
      return true;
    }
    else{
      return false;
    }
  }
    $scope.checkOwnerComment = function(comment){
    if (comment.AuthorID == UserID){
      return true;
    }
    else{
      return false;
    }
  }

  $scope.makeComment = function(post, comm){
    var postID = post.PostID;
    var jsonPost = {
      UserID,
      comm,
      postID
    }
    $http.post('/makeComment', jsonPost).success(function(res){
      if (res == "success"){
        getPostData();
      }
      else{
        alert("Comment failed")
      }
    })
  }
  

});


app.controller('editPostCtrl',function($uibModal,$scope,srvShareData,$http){

var data = srvShareData.getData();
$scope.newPost = data[3]
  var getPostData = function(){
    var postDataA = [];
    var anotherData;
    $http.post('/getAllFriendPosts', data[0]).
    success(function(res){
      var  postData = res;
    angular.forEach(postData, function(value,key){
      $http.post('/getPersonalComment', value).
      success(function(res){
        var comment = {Comments: res};
        var createData = angular.extend(value, comment);
        postDataA.push(createData);
        $scope.posts = postDataA;
      })

    })
    //$scope.posts = res;

  })
}

$scope.editPost=function(){

var post = {
  UserID: data[0].UserID,
  PostID: data[3].PostID,
  Content: $scope.newPost.Content
}
  $http.post('/editPost',post).success(function(res){
    if(res == 'success'){
      alert('Edited Post!')

      editPostModal.dismiss('cancel')
    }
  })
}

$scope.editGroupPost=function(){

var post = {
  UserID: data[0].UserID,
  PostID: data[3].PostID,
  Content: $scope.newPost.Content
}
  $http.post('/editGroupPost',post).success(function(res){
    if(res == 'success'){
      alert('Edited Post!')

      editPostModal.dismiss('cancel')
    }
  })
}

$scope.close=function(){
  editPostModal.dismiss('cancel')
}
})

app.controller('editCommentCtrl',function($uibModal,$scope,srvShareData,$http){

var data = srvShareData.getData();
$scope.newPost = data[4]
  var getPostData = function(){
    var postDataA = [];
    var anotherData;
    $http.post('/getAllFriendPosts', data[0]).
    success(function(res){
      var  postData = res;
    angular.forEach(postData, function(value,key){
      $http.post('/getPersonalComment', value).
      success(function(res){
        var comment = {Comments: res};
        var createData = angular.extend(value, comment);
        postDataA.push(createData);
        $scope.posts = postDataA;
      })

    })
    //$scope.posts = res;

  })
}

$scope.editComment=function(){

var post = {
  UserID: data[0].UserID,
  CommentID: data[4].CommentID,
  Content: $scope.newPost.Content
}
  $http.post('/editComment',post).success(function(res){
    if(res == 'success'){
      alert('Edited Comment!')

      editPostModal.dismiss('cancel')
    }
  })
}

$scope.editGroupComment=function(){
var post = {
  UserID: data[0].UserID,
  CommentID: data[4].CommentID,
  Content: $scope.newPost.Content
}
  $http.post('/editGroupComment',post).success(function(res){
    if(res == 'success'){
      alert('Edited sdfdsfef!')

      editPostModal.dismiss('cancel')
    }
  })
}

$scope.close=function(){
  editPostModal.dismiss('cancel')
}
})


app.controller('messageCtrl', function($uibModal,$scope,srvShareData, $http){
  $scope.animationsEnabled = true;
  $scope.sendAMessage = function(size){
    sendMsgModal = $uibModal.open({
      templateUrl: 'newMsg.html',
      controller:'sendMessageCtrl',
      size: size
    })
  }

  $scope.seeMessages = function(size){
    msgModal = $uibModal.open({
      templateUrl: 'allMessages.html',
      controller: 'allMessagesCtrl',
    })
  }
})

app.controller('allMessagesCtrl', function($uibModal,$scope,srvShareData,$http){
  var sharedData = srvShareData.getData();
  var userID = sharedData[0].UserID
  var post = {
    userID: userID
  }
  $http.post('/getAllMessages', post).success(function(res){
    var messages = []
    for (var i = 0; i<res.length; i++){
      var message = {
        MessageID: res[i].MessageID,
        SendDate: res[i].SendDate,
        SenderID: res[i].SenderID,
        ReceiverID: res[i].ReceiverID,
        Subject: res[i].Subject,
        Content: res[i].Content
      }
      messages.push(message)
    }
    $scope.messages = messages;
  }) 

  $scope.deleteMessage=function(message){
    $http.post('/deleteMessage', message).success(function(res){
      if(res == "success"){
        alert("Message deleted");
      }
      else{
        alert("Failed to delete Message")
      }
      $http.post('/getAllMessages', post).success(function(res){
      var messages = []
      for (var i = 0; i<res.length; i++){
      var message = {
        MessageID: res[i].MessageID,
        SendDate: res[i].SendDate,
        SenderID: res[i].SenderID,
        ReceiverID: res[i].ReceiverID,
        Subject: res[i].Subject,
        Content: res[i].Content
      }
      messages.push(message)
    }
    $scope.messages = messages;
  }) 

    })
  }


  $scope.close = function(){
    msgModal.dismiss('cancel')
  }
})


app.controller('sendMessageCtrl', function($uibModal,$scope,srvShareData, $http){
  var sharedData = srvShareData.getData();
  var senderID = sharedData[0].UserID


  $scope.sendMsg = function(){

      var post = {
    senderID : senderID,
    receiverID : $scope.recipientID,
    message : $scope.messageToSend,
    subject : $scope.subject
  }
    
    $http.post('/sendMsg', post).success(function(res){
      if(res == "success"){
        alert("Message sent!")
      }
      else{
        alert("Message failed")
      }
      sendMsgModal.dismiss('cancel')
    })
  }


  $scope.close = function(){
    sendMsgModal.dismiss('cancel')
  }
})


app.controller('adControl', function($uibModal, $scope,srvShareData, $http){
  $scope.animationsEnabled = true;

    var Info = srvShareData.getData();

    $http.post('/checkEmployee', Info[0]).
    success(function(res){
      if(res == "1")
        $scope.isEmp = false;
      else
        $scope.isEmp = true;
    })
        

  $scope.createNewAd = function(size){
    createAdModal = $uibModal.open({
      templateUrl:'createAd.html',
      controller:'createAdCtrl',
      size: size
    })
  }

  $scope.deleteAd = function(size){
    deleteAdModal = $uibModal.open({
      templateUrl:'deleteAd.html',
      controller:'deleteAdCtrl',
      size: size
    })
  }
  $scope.addSale = function(size){
    deleteAdModal = $uibModal.open({
      templateUrl:'addSale.html',
      controller:'adControl',
      size: size
    })
  }

  $scope.createAcc = function(size){
    deleteAdModal = $uibModal.open({
      templateUrl:'createAcc.html',
      controller:'CustomerCtrl',
      size: size
    })
  }  
  $scope.deleteAcc = function(size){
    deleteAdModal = $uibModal.open({
      templateUrl:'deleteAcc.html',
      controller:'CustomerCtrl',
      size: size
    })
  }  
  $scope.editAcc = function(size){
    deleteAdModal = $uibModal.open({
      templateUrl:'editAcc.html',
      controller:'CustomerCtrl',
      size: size
    })
  }  

  $scope.retriveEmail = function(size){
      deleteAdModal = $uibModal.open({
      templateUrl:'getEmail.html',
      controller:'getEmailCtrl',
      size: size
    })
  }

  $scope.retriveCGroup = function(size){
      deleteAdModal = $uibModal.open({
      templateUrl:'getGroupC.html',
      controller:'getGroupCCtrl',
      size: size
    })
  }

  $scope.retriveAccHistory = function(size){
      deleteAdModal = $uibModal.open({
      templateUrl:'getAccHistory.html',
      controller:'getAccHistoryCtrl',
      size: size
    })
  }

  $scope.generateSuggestion = function(size){
      deleteAdModal = $uibModal.open({
      templateUrl:'generateSuggestion.html',
      controller:'generateSuggestionCtrl',
      size: size
    })
  }
  $scope.getBestSell = function(size){
      deleteAdModal = $uibModal.open({
      templateUrl:'getBestSell.html',
      size: size
    })
  }

  $scope.generatePersonalSugg = function(size){
      deleteAdModal = $uibModal.open({
      templateUrl:'personalSug.html',
      controller:'personalSugCtrl',
      size: size
    })
  }

  $scope.recordSale = function(){
    var post= {
      AccountNumber: $scope.AccountNumber,
      AdvertisementID: $scope.AdvertisementID,
      Qty: $scope.Quantity
    }
    $http.post('/addSale', post).
    success(function(res){
      if(res =="error")
        $scope.info = "Fail to record transaction"
      else if(res =="1")
        $scope.info = "AccountNumber not exist"
      else if(res =="2")
        $scope.info = "AdvertisementID not exist"
      else if(res == "3")
        $scope.info = "You have record the transaction"

    })
  }

  $scope.close = function(){
    createAdModal.dismiss('cancel')
  }
})

app.controller('personalSugCtrl', function($uibModal,$scope,srvShareData, $http){
  $scope.generate = function(){
    var post = {
      AccountNumber: $scope.AccountNumber
    }
    $http.post('/personalSug', post).
    success(function(res){
      $scope.suggestions = res
    })
  }
})

app.controller('getBestSellCtrl', function($uibModal,$scope,srvShareData, $http){
  $http.post('/getBestSell', null).
  success(function(res){
    $scope.bests = res;
  })
})

app.controller('getEmailCtrl', function($uibModal,$scope,srvShareData, $http){
  $http.post('/getEmail',null).
  success(function(res){
    $scope.emails = res;
  })
})

app.controller('generateSuggestionCtrl', function($uibModal,$scope,srvShareData, $http){
  $scope.generate = function(){
    var post ={
      AccountNumber:$scope.AccountNumber
    }
    $http.post('/generateSuggestion', post).
    success(function(res){
      $scope.suggestions = res
    })
  }
})

app.controller('getAccHistoryCtrl', function($uibModal,$scope,srvShareData, $http){
  $scope.getAccountHis = function(){
    var post = {
      UserID: $scope.UserID
    }
      $http.post('/getAccHistory', post).
  success(function(res){
    $scope.historys = res;
  })
  }

})

app.controller('getGroupCCtrl',function($uibModal,$scope,srvShareData, $http){
  $scope.getAccGroup = function(){
    var post = {
      AccountNumber: $scope.AccNumber
    }
  $http.post('/getCGroup', post).
  success(function(res){
    if(res == "1")
      $scope.info = "Account not found"
    else{
      $scope.groups = res;
      
    }
  })
}
})

app.controller('CustomerCtrl', function($uibModal,$scope,srvShareData, $http){
  var userInfo = srvShareData.getData();
  $scope.hidInfo = true;

  $scope.createAccount = function(){

    var post = {
      AccountNumber: $scope. AccountNumber,
      UserID: $scope.UserID
    }
    $http.post('/createAcc', post).
    success(function(res){
      if(res =="error")
        $scope.info = "Fail to record transaction"
      else if(res =="1")
        $scope.info = "AccountNumber already exist"
      else if(res =="2")
        $scope.info = "UserID not exist"
      else if(res == "3")
        $scope.info = "You have Create a account for user "+$scope.UserID

    })

  }

    $scope.createAccount = function(){

    var post = {
      AccountNumber: $scope. AccountNumber,
      UserID: $scope.UserID
    }
    $http.post('/createAcc', post).
    success(function(res){
      if(res =="error")
        $scope.info = "Fail to create Account"
      else if(res =="1")
        $scope.info = "AccountNumber already exist"
      else if(res =="2")
        $scope.info = "UserID not exist"
      else if(res == "3")
        $scope.info = "You have Create a account for user "+$scope.UserID

    })

  }

  $scope.deleteAccount = function(){
    var post = {
      AccountNumber: $scope.AccountNumber
    }
    $http.post('/deleteAcc', post).
    success(function(res){
      if(res =="error")
        $scope.info = "Fail to delete account"
      else if(res =="1")
        $scope.info = "AccountNumber not exist"
      else if(res =="2")
        $scope.info = "you have delete account " + $scope.AccountNumber

    })
  }
  var savedAcc;
  $scope.searchAccount = function(){
    var post = {
      AccountNumber: $scope.AccountNumber
    }
    $http.post('/searchAcc', post).
    success(function(res){
      if(res == "1")
        $scope.info = "AccountNumber not Found"
      else{
        $scope.hidInfo = false;
        $scope.newAccountNum = res[0].AccountNumber;
        $scope.newUserID = res[0].UserID;
        savedAcc = res[0].AccountNumber;
      }
    })
  }

  $scope.editAccount = function(){
    var post = {
      oldAcc: savedAcc,
      newAcc : $scope.newAccountNum,
      newUserID: $scope.newUserID
    }

    $http.post('/editAcc', post).
    success(function(res){
      if(res =="error")
        $scope.info = "Fail to edit account"
      else if(res =="1")
        $scope.info = "UserID not found"
      else if(res =="2")
        $scope.info = "you have update the account info for "+ $scope.oldAcc
    })
  }

  $scope.close = function(){
    deleteAdModal.dismiss('cancel')
  }
})

app.controller('createAdCtrl', function($uibModal,$scope,srvShareData, $http){
  var userInfo = srvShareData.getData();
  $scope.adType = {
    type: "select",
    option: "Automotive",
    options: [
      'Automotive',
      'Technology',
      'Home Goods',
      'Academics',
      'Sports',
    ],
    selected: 'Autmotive'
  };

  $scope.createAd = function(){
   var post = {
      Company: $scope.CompanyName,
      Type: $scope.adType.option,
      ItemName: $scope.ItemName,
      Content: $scope.Content,
      EmployeeId: userInfo[0].UserID,
      UnitPrice: $scope.UnitPrice,
      Qty: $scope.Quantity
    }
    $http.post('/createAd', post).success(function(res){
      if(res == "success"){
        alert("Ad created!")
      }
      else{
        alert("Failed creating Ad")
      }
    })
  };

  $scope.close = function(){
    createAdModal.dismiss('cancel')
  }
});

app.controller('deleteAdCtrl', function($uibModal,$scope,srvShareData, $http){
  var data = [];
  $http.post('/getAllAds').
  success(function(res){
    var adsArray = []
    for(var x = 0; x<res.length; x++){
      adsArray.push(res[x].AdvertisementID)
      data.push(res[x])
    }
    $scope.ads = adsArray
  });

  $scope.changedValue = function(index){
    for(x in data){
      if(data[x].AdvertisementID == index){
        $scope.cName = "Company name: " + data[x].Company
        $scope.Type = "Type of Ad: " + data[x].Type
        $scope.Content = "Ad Content: " + data[x].Content
      }
    }
  }

  $scope.deleteAd = function(adSelected){
    var post = {
      adSelected
    }
    $http.post('/deleteAd',post).
    success(function(res){
        if(res == "success"){
          alert("Deleted Ad!")
        }
        else{
          alert("Failed in deleting Ad")
        }
    })
  }


  $scope.close = function(){
    deleteAdModal.dismiss('cancel')
  }

})



app.controller('groupPostCtrl', function ($uibModal, $log, $document, $scope, srvShareData, $location, $http) {
    var Info = srvShareData.getData();
  $scope.checkOwnerPost = function(post){
    if (post.AuthorID == Info[0].UserID){
      return true;
    }
    else{
      return false;
    }
  }
  $scope.checkOwnerComment = function(comment){
    if (comment.AuthorID == Info[0].UserID){
      return true;
    }
    else{
      return false;
    }
  }
  var checkGroupOwner = function(){
  $http.post('/checkOwner', Info).
  success(function(res){
    if(res == "1")
     return true
    else if(res == "3")
      return false
    else
      return false
  });
  }

    $scope.checkOwnerGPost = function(post){
    if ((post.AuthorID == Info[0].UserID) || (Info[0].UserID == Info[1].Owner)){

      return true;
    }
    else
      return false;
  }

      $scope.checkOwnerGComment = function(comment){
    if ((comment.AuthorID == Info[0].UserID) || (Info[0].UserID == Info[1].Owner)){

      return true;
    }
    else
      return false;
  }


    $scope.editPost = function(item,size){
      srvShareData.editPost(item)
      editPostModal = $uibModal.open({
      templateUrl: 'editPost.html',
      controller:'editPostCtrl',
      size: size
    })
      
  }
    $scope.editComment = function(item,size){
      srvShareData.editComment(item)
      editPostModal = $uibModal.open({
      templateUrl: 'editComment.html',
      controller:'editCommentCtrl',
      size: size
    })
      
  }


  var getGroupPostData = function(){
      var postDataA = []
    $http.post('/getGroupPost', Info[1]).
    success(function(res){
      var postData = res;
      angular.forEach(postData, function(value, key){
        $http.post('/getGroupComment', value).
        success(function(res){
          var comment = {Comment: res};
          var createData = angular.extend(value, comment);
          postDataA.push(createData);
          $scope.GroupPost = postDataA;
        })
      })
    })
  }
  $scope.deleteGPost = function(post){
    $http.post('/deleteGPost', post).
    success(function(res){
      if(res =="error")
        alert("Delete post failed");
      else
        getGroupPostData();
    })
  }
  $scope.deleteGComment = function(comment){

    $http.post('/deleteGComment', comment).
    success(function(res){
      if(res =="error")
        alert("Delete post failed");
      else
        getGroupPostData();
    })
  }

  getGroupPostData();
  $scope.makeGroupPost = function(){
    var jsonPost = {
      PostInfo: Info,
      Content: $scope.postToMake
    }
    
    $http.post('/makeGroupPost', jsonPost).
    success(function(res){
      if(res == "error")
        $scope.postInfo = "Fail to post"
      else{
        $scope.postInfo = "Success"
      getGroupPostData();
    }
    })
  }

   $scope.likePost = function(post){
    var postID = post.PostID;

    var counter = '+1';
    var reqToSend = {
        postID,
        counter
    };
   $http.post('/updateGroupPostLike', reqToSend).success(function(res){
      if(res == "success"){
        getGroupPostData();
      }
      else{
        alert("Liking Post failed")
      }
    })
  };

 $scope.unlike = function(post){
    var postID = post.PostID;
    var counter = '-1';
    var reqToSend = {
        postID,
        counter
    };
   $http.post('/updateGroupPostLike', reqToSend).success(function(res){
      if(res == "success"){
        getGroupPostData();
      }
      else{
        alert("Liking Post failed")
      }
    })
  }

  $scope.makeComment = function(post, comm){
    var postID = post.PostID;
    UserID = Info[0].UserID;
    var jsonPost = {
      UserID,
      comm,
      postID
    }
    $http.post('/makeGroupComment', jsonPost).success(function(res){
      if (res == "success"){
         getGroupPostData();
      }
      else{
        alert("Comment failed")
      }
    })
  }

    $scope.likeComment = function(comment){
    var commentID = comment.CommentID;
    var counter = '+1';
    var reqToSend = {
      commentID,
      counter
    };
    $http.post('/updateGroupCommentLike', reqToSend).
    success(function(res){
      if(res == "1"){
        getGroupPostData();
      }
      else{
        alert("Likeing comment failed")
      }
    })

  };
    $scope.unlikeComment = function(comment){
    var commentID = comment.CommentID;
    var counter = '-1';
    var reqToSend = {
      commentID,
      counter
    };
    $http.post('/updateGroupCommentLike', reqToSend).
    success(function(res){
      if(res == "1"){
        getGroupPostData();
      }
      else{
        alert("Likeing comment failed")
      }
    })

  };

})




app.controller('GroupCtrl', function ($uibModal, $log, $document, $scope, srvShareData, $location, $http) {
  $scope.oneAtATime = true;
  $scope.animationsEnabled = true;
  var userInfo = srvShareData.getData();


  $scope.retriveData = function(){
      $http.post('/group', userInfo[0]).success(function(res){
    $scope.groups = res;

  });
  };


  $scope.joinGroup = function(size, parentSelector){
      var parentElem = parentSelector ? 
      angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
      createGroupModal = $uibModal.open({
        animation: $scope.animationsEnabled,
        ariaLabelledBy: 'modal-title',
        ariaDescribedBy: 'modal-body',
        templateUrl: 'joinGroup.html',
        controller: 'joinGroupCtrl',
        controllerAs: '$ctrl',
        size: size,
        appendTo: parentElem,
  
      });
    }

    $scope.unjoinGroup = function(size, parentSelector){
      var parentElem = parentSelector ? 
      angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
      createGroupModal = $uibModal.open({
        animation: $scope.animationsEnabled,
        ariaLabelledBy: 'modal-title',
        ariaDescribedBy: 'modal-body',
        templateUrl: 'unjoinGroup.html',
        controller: 'unjoinGroupCtrl',
        controllerAs: '$ctrl',
        size: size,
        appendTo: parentElem,
  
      });
    }

    $scope.goToGroup = function(index){
      $scope.groupInfo=[];
      $scope.groupInfo.push($scope.groups[index].GroupID);
      //var test = {GroupID: .GroupID};

      
      srvShareData.addGroup($scope.groups[index]);
      var temp = srvShareData.getData();
      window.location.href = "group.html";
    }
    $scope.createGroup = function (size, parentSelector) {
      var parentElem = parentSelector ? 
        angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
       createGroupModal = $uibModal.open({
        animation: $scope.animationsEnabled,
        ariaLabelledBy: 'modal-title',
        ariaDescribedBy: 'modal-body',
        templateUrl: 'createGroup.html',
        controller: 'createGroupCtrl',
        controllerAs: '$ctrl',
        size: size,
        appendTo: parentElem,
  
      });
  

    };



});

app.controller('createGroupCtrl', function($uibModal, $scope,srvShareData, $http){
  $scope.groupType = {
    options: [
      'Club',
      'Organization',
      'Association',
      'Work',
      'Personal',
      'Interest'
    ],
    selected: 'Club'
  };

  var userInfo = srvShareData.getData();
  //$scope.createGroup.userInfo[0];

  $scope.sub = function(){

      var createData = angular.extend($scope.CreateGroup, userInfo[0]);

    $http.post('/createGroup', createData).
    success(function(res){
      $scope.info = "New page created! You can now close the window";
    })
  }

  $scope.close= function(){
    createGroupModal.dismiss('cancel');
  }
});

app.controller('joinGroupCtrl', function($uibModal, $scope,srvShareData, $http){
  var data;
  $http.post('/getGroupData', "").
  success(function(res){
    data = res;
    var test = [];
    for(x in res)
      test.push(res[x].GroupID);
    $scope.groups = test;
  });

  var userInfo = srvShareData.getData();
  //$scope.createGroup.userInfo[0];
  $scope.selectedValue = function(index){
    $scope.gname = "Group Name: " + data[index-1].GroupName;
    $scope.type = "Group Type: " + data[index-1].Type;
    $scope.name = "Group Owner: " + data[index-1].FirstName + " " +data[index-1]. LastName;
  }
  $scope.sub = function(){
    var data = {GroupID: $scope.GroupSelected};
     var joinData = angular.extend(data, userInfo[0]);
    $http.post('/joinGroup', joinData).
    success(function(res){
      if(res == "3")
        $scope.info = "Already in the Group";
      else if(res =="1")
        $scope.info = "You have join the group! Now you can close the window!";
      else
        $scope.info = "Error";
    })
  }

  $scope.close= function(){
    createGroupModal.dismiss('cancel');
  }
});

app.controller('unjoinGroupCtrl', function($uibModal, $scope,srvShareData, $http){
  var data;
  var userInfo = srvShareData.getData();

  $http.post('/getJoinedGroupData', userInfo[0]).
  success(function(res){
    data = res;
    var test = [];
    for(x in res)
      test.push(res[x].GroupID);
    $scope.groups = test;
  });


  //$scope.createGroup.userInfo[0];
  $scope.selectedValue = function(index){
  for(x in data){
    if(data[x].GroupID == index){
        $scope.gname = "Group Name: " + data[x].GroupName;
        $scope.type = "Group Type: " + data[x].Type;
        $scope.name = "Group Owner: " + data[x].FirstName + " " +data[x]. LastName;
    }
  }

  }
  $scope.sub = function(){
    var data = {GroupID: $scope.GroupSelected};
     var joinData = angular.extend(data, userInfo[0]);
    $http.post('/unjoinGroup', joinData).
    success(function(res){
      if(res =="1")
        $scope.info = "You have unjoin the group! Now you can close the window!";
      else if(res =="3")
        $scope.info = "You are not in the group!";
      else
        $scope.info = "Error";
    })
  }

  $scope.close= function(){
    createGroupModal.dismiss('cancel');
  }
});

app.controller('groupPageCtrl', function ($uibModal, $log, $document, $scope, srvShareData, $location, $http) {
  var info = srvShareData.getData();
  $scope.GroupTitle = info[1].GroupName;
  $http.post('/checkOwner', info).
  success(function(res){
    if(res == "1")
      $scope.mgnHide=false;
    else if(res == "3")
      $scope.mgnHide=true;
    else
      $scope.mgnHide=true;

  });

});

app.controller('GroupMgnCtrl', function ($uibModal, $log, $document, $scope, srvShareData, $location, $http) {
  var info = srvShareData.getData();
  var userinfo;
  $scope.addUser = function (size, parentSelector) {
      var parentElem = parentSelector ? 
        angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
       createGroupModal = $uibModal.open({
        animation: $scope.animationsEnabled,
        ariaLabelledBy: 'modal-title',
        ariaDescribedBy: 'modal-body',
        templateUrl: 'addUser.html',
        controller: 'addUserCtrl',
        size: size,
        appendTo: parentElem,
  
      });
    };

    $scope.removeUser = function (size, parentSelector) {
      var parentElem = parentSelector ? 
        angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
       createGroupModal = $uibModal.open({
        animation: $scope.animationsEnabled,
        ariaLabelledBy: 'modal-title',
        ariaDescribedBy: 'modal-body',
        templateUrl: 'removeUser.html',
        controller: 'removeUserCtrl',
        size: size,
        appendTo: parentElem,
  
      });
    };
    $scope.renameGroup = function (size, parentSelector) {
      var parentElem = parentSelector ? 
        angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
       createGroupModal = $uibModal.open({
        animation: $scope.animationsEnabled,
        ariaLabelledBy: 'modal-title',
        ariaDescribedBy: 'modal-body',
        templateUrl: 'renameGroup.html',
        controller: 'renameGroupCtrl',
        size: size,
        appendTo: parentElem,
  
      });
    };

    $scope.deleteGroup = function (size, parentSelector) {
      var parentElem = parentSelector ? 
        angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
       createGroupModal = $uibModal.open({
        animation: $scope.animationsEnabled,
        ariaLabelledBy: 'modal-title',
        ariaDescribedBy: 'modal-body',
        templateUrl: 'deleteGroup.html',
        controller: 'deleteGroupCtrl',
        size: size,
        appendTo: parentElem,
  
      });
    };


});

app.controller('addUserCtrl', function($uibModal, $scope,srvShareData, $http){
  var searchedUser;
  $scope.searched = true;
  $scope.searchUser = function(){
      var test = {UserID: $scope.UserID};
      $http.post('/searchUser', test).
      success(function(res){
        if(res=="fail")
          $scope.info = "Error";
        else if(res == "3")
          $scope.info = "User not found!";
        else{
          searchedUser = res[0].UserID;
        $scope.UserIDx = "ID: "+ res[0].UserID;
          $scope.UserName = "Name: "+ res[0].FirstName + " "+ res[0].LastName;
          $scope.UserEmailx = "Email: "+ res[0].EmailAddress;           
        }


      })
      $scope.searched = false;
    }

    $scope.sub = function(){
      var groupInfo = srvShareData.getData();
      var postInfo = {UserID: searchedUser, GroupID: groupInfo[1].GroupID};
      $http.post('/AddUserToGroup', postInfo).
      success(function(res){
        if(res =="fail1")
          $scope.info = "Error1";
        else if(res =="fail2")
          $scope.info = "error2";
        else if(res == "3")
          $scope.info = "User already in the group!";
        else
          $scope.info = "You have added the user. You can now close the window";
      })
    }

      $scope.close= function(){
    createGroupModal.dismiss('cancel');
  }
});

app.controller('removeUserCtrl', function($uibModal, $scope,srvShareData, $http){
  var searchedUser;
  $scope.searched = true;
  $scope.searchUser = function(){
      var test = {UserID: $scope.UserID};
      $http.post('/searchUser', test).
      success(function(res){
        if(res=="fail")
          $scope.info = "Error";
        else if(res == "3")
          $scope.info = "User not found!";
        else{
          searchedUser = res[0].UserID;
        $scope.UserID = "ID: "+ res[0].UserID;
          $scope.UserName = "Name: "+ res[0].FirstName + " "+ res[0].LastName;
          $scope.UserEmailx = "Email: "+ res[0].EmailAddress;           
        }


      })
      $scope.searched = false;
    }

    $scope.sub = function(){
      var groupInfo = srvShareData.getData();
      var postInfo = {UserID: searchedUser, GroupID: groupInfo[1].GroupID};
      $http.post('/removeUser', postInfo).
      success(function(res){
        if(res =="fail1")
          $scope.info = "Error1";
        else if(res =="fail2")
          $scope.info = "error2";
        else if(res == "3")
          $scope.info = "User is not in the group!";
        else
          $scope.info = "You have removed the user. You can now close the window";
      })
    }

      $scope.close= function(){
    createGroupModal.dismiss('cancel');
  }
});

app.controller('renameGroupCtrl', function($uibModal, $scope,srvShareData, $http){
  var searchedUser;
  $scope.searched = true;

    $scope.sub = function(){
      var groupInfo = srvShareData.getData();
      var postInfo = {newGroupName: $scope.newGroupName, GroupID: groupInfo[1].GroupID};
      $http.post('/renameGroup', postInfo).
      success(function(res){
        if(res =="fail")
          $scope.info = "Error";
        else{
          $scope.info = "You have rename the group. You can now close the window";
        var newData = groupInfo[1];
        newData.GroupName = $scope.newGroupName;
        srvShareData.addGroup(newData);  
        window.location.href = "group.html";        
        }
      })
    }

      $scope.close= function(){
    createGroupModal.dismiss('cancel');
  }
});

app.controller('deleteGroupCtrl', function($uibModal, $scope,srvShareData, $http){
  var searchedUser;
  $scope.searched = true;

    $scope.sub = function(){
      var groupInfo = srvShareData.getData();
      var postInfo = {GroupID: groupInfo[1].GroupID};
      $http.post('/deleteGroup', postInfo).
      success(function(res){
        if(res =="Faile to empty group data")
          $scope.info = "Faile to empty group data";
        else{
          if(res == "1"){
            $scope.info = "You have delete the group. You can now close the window :(";
            window.location.href = "mainPage.html"; 
          }
          else
            $scope.info = "Error";
              
        }
      })
    }

      $scope.close= function(){
    createGroupModal.dismiss('cancel');
  }
});



app.controller('mngrCtrl',function($scope,$location,$http, $uibModal){

  $scope.getAdInfo =function(size){
    adListModal = $uibModal.open({
      templateUrl: 'adList.html',
      controller: 'adListCtrl',
    })
  }

});

app.controller('adListCtrl',function($scope,$location,$http,$uibModal){

  $http.post('/getAllAds').
  success(function(res){
  
  });
});



app.service('srvShareData', function($window) {
        var KEY = 'App.SelectedValue';

        var addData = function(newObj) {
            var mydata = $window.sessionStorage.getItem(KEY);
            if (mydata) {
                mydata = JSON.parse(mydata);
            } else {
                mydata = [];
            }
            mydata[0] = newObj;
            $window.sessionStorage.setItem(KEY, JSON.stringify(mydata));
        };
        var addGroup = function(newObj) {
            var mydata = $window.sessionStorage.getItem(KEY);
            if (mydata) {
                mydata = JSON.parse(mydata);
            } else {
                mydata = [];
            }
            mydata[1] = newObj;
            $window.sessionStorage.setItem(KEY, JSON.stringify(mydata));
        };
        var addFriend = function(newObj) {
            var mydata = $window.sessionStorage.getItem(KEY);
            if (mydata) {
                mydata = JSON.parse(mydata);
            } else {
                mydata = [];
            }
            mydata[2] = newObj;
            $window.sessionStorage.setItem(KEY, JSON.stringify(mydata));
        };

        var getData = function(){
            var mydata = $window.sessionStorage.getItem(KEY);
            if (mydata) {
                mydata = JSON.parse(mydata);
            }
            return mydata || [];
        };

        var editPost = function(newObj){
           var mydata = $window.sessionStorage.getItem(KEY);
            if (mydata) {
                mydata = JSON.parse(mydata);
            } else {
                mydata = [];
            }
            mydata[3] = newObj;
            $window.sessionStorage.setItem(KEY, JSON.stringify(mydata));
        };     
                var editComment = function(newObj){
           var mydata = $window.sessionStorage.getItem(KEY);
            if (mydata) {
                mydata = JSON.parse(mydata);
            } else {
                mydata = [];
            }
            mydata[4] = newObj;
            $window.sessionStorage.setItem(KEY, JSON.stringify(mydata));
        };    
        return {
          addGroup: addGroup,
            addData: addData,
            getData: getData,
            addFriend: addFriend,
            editPost: editPost,
            editComment: editComment
        };
    });

